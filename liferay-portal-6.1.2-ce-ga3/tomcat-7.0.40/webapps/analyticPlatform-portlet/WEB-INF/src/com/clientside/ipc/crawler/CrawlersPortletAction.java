/*
 @create on	:	    1/04/2014
 
 @author	:		Konstantinos Pechlivanis
 
 @research program: PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 
 @project	:		Analytic Platform (Work package 2)
  		-Task 2.1:  Development of Scientific means 
  		-Task 2.3:  Technical Platform
 
 @document: 		CrawlerPortletAction.java from file /WEB-INF/src/com/clientside/ipc/crawler/CrawlerPortletAction.java
 
 @summary	:		This class handles the items and the name of crawler that are received from manage_crawler portlet and send them to receiver portlet.
 					Depend on selection of crawler (this class) send to the receiver portlet the date and time of activation which read them from a file
*/

package com.clientside.ipc.crawler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CrawlersPortletAction extends MVCPortlet {

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String crawler = ParamUtil.getString(resourceRequest,"termCrawl");
		Logger myLog = Logger.getLogger(getClass().getName());
		System.out.println("crontab -u " + System.getProperty("user.name") + " /home/"+System.getProperty("user.name")+"/.PrepareData/crontab_unix.txt");
		
		PrintWriter pw = resourceResponse.getWriter();
		JSONObject juser = JSONFactoryUtil.createJSONObject();
		
		if (crawler.contains(",left")){// left click
			crawler = crawler.replace(",left", "").trim();
			JSONArray array = JSONFactoryUtil.createJSONArray();
			JSONArray arrayFig = JSONFactoryUtil.createJSONArray();
			
			try {
				File file = new File("/home/"+System.getProperty("user.name") + "/.PrepareData" + "/crawler_" + crawler + ".txt");
				File fileCron = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/crawler_crontab.txt");
				array.put(crawler);		// add in the first position of array the name of selected crawler
				
//-------------------------------------------------Activate/Inactive time and num_docs--------------------------------------------------------------------				
				myLog.info("-------- MySQL JDBC Connection Testing ------------");
				
				// return from database the time of activation and inactivation
				Properties properties = new Properties();	// set properties of database
			    properties.put("user","searchuser");
			    properties.put("password","searchUser_@!");
			    properties.put("characterEncoding", "UTF-8");
			    
			    try {
			    // Class.forName(xxx) loads the jdbc classes and creates a drivermanager class factory
			    Class.forName("com.mysql.jdbc.Driver");
			    
			    } catch (ClassNotFoundException e) {
			    	myLog.info("Where is your MySQL JDBC Driver?");
					e.printStackTrace();
					return;
				}
			    myLog.info("MySQL JDBC Driver Registered!");
			    Connection conn = null;
			    
			    try {
					conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prepare_crawler_db",properties);
				} catch (SQLException e) {
					myLog.info("Connection Failed! Check output console");
					e.printStackTrace();
					return;
				}
				if (conn != null)
					myLog.info("You made it, take control your database now!");
				else
					myLog.info("Failed to make connection!");
			    
			    Statement st = conn.createStatement();
			    ResultSet rs = st.executeQuery("select crawl_id, start_date, end_date, num_docs from crawl_log where crawl_name='" + crawler + "' order by start_date asc");

			    String endDate="", startDate="", num_docs="", crawl_id = "0";
			    while(rs.next()){
			    	crawl_id = rs.getString(1);
			    	startDate = rs.getString(2);
			    	endDate = rs.getString(3);
			    	num_docs = rs.getString(4);
			    	arrayFig.put(rs.getString(4));
			    }
			    
			    rs = st.executeQuery("select crawler_name, num_docs from crawl_query_lkp where crawl_id='" + crawl_id + "'");// get crawl_id, end_date and num_docs from database
			    
			    int bing=0, gplus=0, youtube=0;
			    while(rs.next()){
			    	if (rs.getString(1).equals("BingCrawler"))
			    		bing += Integer.parseInt(rs.getString(2));
			    	else if (rs.getString(1).equals("GplusCrawler"))
			    		gplus += Integer.parseInt(rs.getString(2));
			    	else if (rs.getString(1).equals("YoutubeCrawler"))
			    		youtube += Integer.parseInt(rs.getString(2));
			    }
			    
			    st.close();conn.close();
			    if (startDate==""){
			    	array.put("Not activate");
			    	array.put("Not activate");
			    	array.put("Not activate");
			    	array.put("Not activate");
			    	array.put("Not activate");
			    	array.put("Not activate");
			    }else{
			    	array.put("Date: " + startDate.split(" ")[0] + " Time:" + startDate.split(" ")[1]);
			    	if (endDate!=null){
				    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
				    }else
				    	array.put("Not available yet");
			    	array.put("Bing: " + String.valueOf(bing));
					array.put("Google Plus: " + String.valueOf(gplus));
					array.put("Youtube: " + String.valueOf(youtube));
					array.put("Total documents: " + num_docs);
			    }
//-------------------------------------------------CRONTAB--------------------------------------------------------------------
				if (fileCron.exists()) {// if file exists
					BufferedReader bf = new BufferedReader(new FileReader(fileCron.getAbsoluteFile()));	// create a buffer for a file
			  		String term;
			  		while ((term = bf.readLine()) != null ){// while there are terms in configuration file
			  			if (term.split("-->")[0].equals(crawler)){
			  				array.put(term.split("-->")[1]);		// add in first position of array the name of selected crawler and the date of activate
			  				break;
			  			}
			  		}bf.close();
			  		
			  		if (array.length()==7)	// if select crawler that is not activate yet
			  			array.put("* * * * *");
				}else
					array.put("* * * * *");
//-------------------------------------------------Inserted terms--------------------------------------------------------------
				
				if (file.exists()) {// if file exists
					BufferedReader bf = new BufferedReader(new FileReader(file.getAbsoluteFile()));	// create a buffer for a file
			  		String term; int id_lang;
			  		while ((term = bf.readLine()) != null ){				// while there are terms in configuration file
			  			id_lang = Integer.parseInt(term.split("#")[1])-1;	// find the language
			  			term = term.split("#")[0].split(",")[id_lang];		// take the inserted term
			  			array.put(term);									// add a term in the array
			  		}
			  		bf.close();
				}
		  	} catch (IOException e) {myLog.error("File does not exist!");
		  	} catch (SQLException ex) { // handle any errors
		  		myLog.error("SQLException: " + ex.getMessage());
		  		myLog.error("SQLState: " + ex.getSQLState());
		  		myLog.error("VendorError: " + ex.getErrorCode());
			}

			juser.put("jsCrawlerTerm", array);
			juser.put("numDocFig", arrayFig);
			pw.println(juser.toString());
			
		}else{// right click
			crawler = crawler.replace(",right", "");
			juser.put("RgClickCr", crawler);
			pw.println(juser);
		}
			
	}
}
