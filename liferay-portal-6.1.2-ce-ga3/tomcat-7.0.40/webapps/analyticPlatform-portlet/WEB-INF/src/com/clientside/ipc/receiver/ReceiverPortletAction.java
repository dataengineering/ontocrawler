/*
 @create on	:	    1/04/2014
 @author	:		Konstantinos Pechlivanis
 @research program: PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project	:		Analytic Platform (Work package 2)
  		-Task 2.1:  Development of Scientific means 
  		-Task 2.3:  Technical Platform
 
 @document	: 		ReceiverPortletAction.java from file /WEB-INF/src/com/clientside/ipc/receiver/ReceiverPortletAction.java

 @summary	:		this class handles the state of crawler (activate, inactivate) and the date of each action and send these information to receiver portlet
*/

package com.clientside.ipc.receiver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.sql.*;
import java.util.Properties;

public class ReceiverPortletAction extends MVCPortlet {
	Hashtable<String, String> crawlerCronTab = new Hashtable<String, String>();//hash table with, key: name of Crawler, value:crontab
	Hashtable<String, ArrayList<String> > crawlerTermHash = new Hashtable<String, ArrayList<String> >();
	Logger myLog = Logger.getLogger(getClass().getName());
	
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String [] activationData = resourceRequest.getParameterValues("myJsonString");
		activationData[0] = activationData[0].replace("\"", "");
		String crawler =  activationData[0].split(",")[0].replace("[", "").trim();		// take the name of crawler
		String state =  activationData[0].split(",")[1].trim();			// take the state of crawler: active/inactive
		String date = activationData[0].split(",")[2].trim();			// take the date of status
		String date_crontab = activationData[0].split(",")[3].replace("]", "").trim();	// take the date of crontab

//----------------------------------------load data----------------------------------------------------------------------------------//
		final File loadFolder = new File("/home/"+System.getProperty("user.name") + "/.PrepareData");
		for (final File fileEntry_cr : loadFolder.listFiles()) {// load all crawlers
			if (!fileEntry_cr.isDirectory()) {// for each file
				if (fileEntry_cr.getName().equals("crawler_crontab.txt")){
					BufferedReader bf = new BufferedReader(new FileReader(fileEntry_cr.getAbsoluteFile()));	// create a buffer for a file
					String tempTerm;
	          		while((tempTerm = bf.readLine()) != null )		// while there are terms in configuration file
	          			crawlerCronTab.put(tempTerm.split("-->")[0].trim(), tempTerm.split("-->")[1].trim());
	          		bf.close();
				 }
			}
		}
//-----------------------------------------------------------------------------------------------------------------------------------//
		for (final File fileEntry : loadFolder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            if (fileEntry.getName().contains("crawler_") && !fileEntry.getName().equals("crawler_crontab.txt")){
	            	String crawlerName = fileEntry.getName().replaceFirst("crawler_","").replace(".txt","");
	            	if (!crawlerTermHash.containsKey(crawlerName)){//add new entry in hash table if crawler does not exist
	    				crawlerTermHash.put(crawlerName, new ArrayList<String>());
	    				BufferedReader bf = new BufferedReader(new FileReader(fileEntry.getAbsoluteFile()));	// create a buffer for a file
		          		String tempTerm;
		          		while((tempTerm = bf.readLine()) != null ) {							// while there are terms in configuration file
		          			tempTerm = tempTerm.split("#")[0];
		          			crawlerTermHash.get(crawlerName).add(tempTerm.split(",")[0]);		// add a term in the hash table
		          			if (!tempTerm.split(",")[0].equals(tempTerm.split(",")[1]))
		          				crawlerTermHash.get(crawlerName).add(tempTerm.split(",")[1]);	// add a term in the hash table
		          			if (!tempTerm.split(",")[1].equals(tempTerm.split(",")[2]))
		          				crawlerTermHash.get(crawlerName).add(tempTerm.split(",")[2]);	// add a term in the hash table
		          		}
		          		bf.close();
	    			}
	            }
	        }
		}
//--------------------------------------------------------------------------------------------------------------------------------//
			
		PrintWriter pw = resourceResponse.getWriter();
		JSONObject juser = JSONFactoryUtil.createJSONObject();
		JSONArray array = JSONFactoryUtil.createJSONArray();
		JSONArray arrayFig = JSONFactoryUtil.createJSONArray();
		array.put(crawler);
		array.put(state);
		
		if (state.equals("Active")){//call ajax for activation of crawler
			try {
				//take the list terms of the specific crawler
				array.put(date);
				array.put(date_crontab);
				
				File fileCrawler = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/crawler_" + crawler + ".txt");
				if (fileCrawler.exists()) {// if file exists
					if (!crawlerTermHash.containsKey(crawler))//add new entry in hash table if crawler does not exist
	    				crawlerTermHash.put(crawler, new ArrayList<String>());

					BufferedReader bf = new BufferedReader(new FileReader(fileCrawler.getAbsoluteFile()));	// create a buffer for a file
					String term;
					while ((term = bf.readLine()) != null ){// while there are terms in configuration file
						term = term.split("#")[0];			//catch only terms
						array.put(term.split(",")[0]);		//insert Greek term
						if(!term.split(",")[1].equals(term.split(",")[0]))	//not insert same term
							array.put(term.split(",")[1]);
						if(!term.split(",")[2].equals(term.split(",")[1]) && !term.split(",")[2].equals(term.split(",")[0]))
							array.put(term.split(",")[2]);
			  		}
					bf.close();
				}
				
				crawlerCronTab.put(crawler, date_crontab); //update cralwer's crontab
				File fileCron = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/crawler_crontab.txt");
				File fileCronUnix = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/crontab_unix.txt");
				if (!fileCron.exists()) // if does not file exists
					fileCron.createNewFile();
				if (!fileCronUnix.exists())// if does not file exists
					fileCron.createNewFile();

				FileWriter fw = new FileWriter(fileCron.getAbsoluteFile());
				FileWriter fwU = new FileWriter(fileCronUnix.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				BufferedWriter bwU = new BufferedWriter(fwU);
				Enumeration<String> keys = crawlerCronTab.keys();	// take the keys of hash table
				while(keys.hasMoreElements()){						// for every key
					String key = keys.nextElement();
					bw.write(key + "-->" + crawlerCronTab.get(key) + "\n");
					if(!crawlerCronTab.get(key).equals("* * * * *")){
						JSONArray arrayUnix = JSONFactoryUtil.createJSONArray();
						for(int i=0; i<crawlerTermHash.get(key).size(); i++){
							arrayUnix.put(crawlerTermHash.get(key).get(i));
						}
						bwU.write(crawlerCronTab.get(key) + " wget --timeout=0 -O - 'http://localhost:8081/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=true&query_list="+ arrayUnix +"&crawl_name=" + key + "' >/dev/null 2>&1");
						bwU.write("\n");
					}
				}
				bw.close();bwU.close();
				Runtime.getRuntime().exec("crontab -u " + System.getProperty("user.name") + " /home/"+System.getProperty("user.name")+"/.PrepareData/crontab_unix.txt");
			} catch (IOException e) {e.printStackTrace();}

//-------------------------------------------------------------------------------------------------------------------------		
		}else if (state.equals("Inactive")){ // call ajax for inactivation of crawler
		    Properties properties = new Properties();	// set properties of database
		    properties.put("user","searchuser");
		    properties.put("password","searchUser_@!");
		    properties.put("characterEncoding", "UTF-8");
			try {
				myLog.info("-------- MySQL JDBC Connection Testing ------------");
				try {
				    // Class.forName(xxx) loads the jdbc classes and creates a drivermanager class factory
				    Class.forName("com.mysql.jdbc.Driver");
				    
				} catch (ClassNotFoundException e) {
					myLog.info("Where is your MySQL JDBC Driver?");
					e.printStackTrace();
					return;
				}
				myLog.info("MySQL JDBC Driver Registered!");
				Connection conn = null;
				try {
					conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prepare_crawler_db",properties);
				} catch (SQLException e) {
					myLog.info("Connection Failed! Check output console");
					e.printStackTrace();
					return;
				}
				if (conn != null)
					myLog.info("You made it, take control your database now!");
				else
					myLog.info("Failed to make connection!");
				
			    Statement st = conn.createStatement();
			    ResultSet rs = st.executeQuery("select crawl_id, end_date, num_docs from crawl_log where crawl_name='" + crawler + "' order by start_date asc");// get crawl_id, end_date and num_docs from database
			    
			    String endDate="",num_docs="0",crawl_id = "0";
			    while(rs.next()){
			    	crawl_id = rs.getString(1);
			    	endDate = rs.getString(2);
			    	num_docs = rs.getString(3);
			    	arrayFig.put(rs.getString(3));
			    }
			    rs = st.executeQuery("select crawler_name, num_docs from crawl_query_lkp where crawl_id='" + crawl_id + "'");// get crawl_id, end_date and num_docs from database
			    
			    int bing=0, gplus=0, youtube=0;
			    while(rs.next()){
			    	if (rs.getString(1).equals("BingCrawler"))
			    		bing += Integer.parseInt(rs.getString(2));
			    	else if (rs.getString(1).equals("GplusCrawler"))
			    		gplus += Integer.parseInt(rs.getString(2));
			    	else if (rs.getString(1).equals("YoutubeCrawler"))
			    		youtube += Integer.parseInt(rs.getString(2));
			    }
			    
			    st.close();conn.close();
			    if (endDate!="" && endDate!=null){
			    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
			    }else
			    	array.put("Not available yet");
				array.put("* * * * *");
				array.put("Bing: " + String.valueOf(bing));
				array.put("Google Plus: " + String.valueOf(gplus));
				array.put("Youtube: " + String.valueOf(youtube));
				array.put("Total documents: " + num_docs);
			} catch (SQLException ex) { // handle any errors
				myLog.info("SQLException: " + ex.getMessage());
				myLog.info("SQLState: " + ex.getSQLState());
				myLog.info("VendorError: " + ex.getErrorCode());
			}
		}
		juser.put("crawlerCond", array);
		juser.put("numDoc", arrayFig);
		pw.println(juser.toString());
	}
}
