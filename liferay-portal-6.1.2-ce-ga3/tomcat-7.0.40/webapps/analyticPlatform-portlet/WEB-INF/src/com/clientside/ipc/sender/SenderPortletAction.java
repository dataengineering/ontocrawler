/*
 @create on	:	   	1/04/2014
 
 @author	:		Konstantinos Pechlivanis
 
 @research program: PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 
 @project	:		Analytic Platform (Work package 2)
  		-Task 2.1:  Development of Scientific means 
  		-Task 2.3:  Technical Platform
 
 @document	: 		SenderPortletAction.java from file /WEB-INF/src/com/clientside/ipc/sender/SenderPortletAction.java

 @summary	:		this class handle the terms: insert or delete terms, write terms in files and save terms in database depend on user's selection
*/

package com.clientside.ipc.sender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class SenderPortletAction extends MVCPortlet {
	Hashtable<String, ArrayList<String> > crawlerTermHash = new Hashtable<String, ArrayList<String> >();
	Hashtable<String, ArrayList<String> > writeTermHash = new Hashtable<String, ArrayList<String> >();
	Logger myLog = Logger.getLogger(getClass().getName());
	
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String term = ParamUtil.getString(resourceRequest,"term");// get the value of string that is received from sender portlet
		String selectedCrawler="", myTerm="", id_lang="";

//----------------------------------------load data----------------------------------------------------------------------------------//
		final File loadFolder = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/");
		for (final File fileEntry : loadFolder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            if (fileEntry.getName().contains("crawler_") && !fileEntry.getName().equals("crawler_crontab.txt")){
	            	String crawlerName = fileEntry.getName().replaceFirst("crawler_","").replace(".txt","");
	            	if (!crawlerTermHash.containsKey(crawlerName)){//add new entry in hash table if crawler does not exist
	    				crawlerTermHash.put(crawlerName, new ArrayList<String>());
	    				BufferedReader bf = new BufferedReader(new FileReader(fileEntry.getAbsoluteFile()));	// create a buffer for a file
		          		String tempTerm;
		          		while((tempTerm = bf.readLine()) != null ){					// while there are terms in configuration file
		          			crawlerTermHash.get(crawlerName).add(tempTerm);			// add a term in the hash table
		          		}
		          		bf.close();
	    			}
	            }
	        }
		}
//--------------------------------------------------------------------------------------------------------------------------------//

		if (term.contains(":") && !term.contains("Remove")){// case of delete a term or write a term
			// take the value of term and the name of crawler
			selectedCrawler = term.split(":")[1].split("--")[1].trim();
			myTerm = term.split(":")[1].split("--")[0].trim();
		}
		else if (term.contains("--")){// case for insert a term
			// take the value of term, the name of crawler and the terms of database
			selectedCrawler = term.split("--")[1].trim();
			myTerm = term.split("--")[0].trim();
			id_lang = myTerm.substring(0, 1);
			myTerm = myTerm.substring(1) + "#" + id_lang;
		}
		else if (term.contains("Remove:")){// case for remove crawler
			selectedCrawler = term.replace("Remove:", "");// take the name of crawler
			myTerm = "";
		}
		
		PrintWriter pw = resourceResponse.getWriter();
		JSONObject juser = JSONFactoryUtil.createJSONObject();
		
		if(!selectedCrawler.equals("") && !myTerm.equals("")){// if exist new selected term
			if (!crawlerTermHash.containsKey(selectedCrawler))// add new entry in hash table if crawler does not exist
				crawlerTermHash.put(selectedCrawler, new ArrayList<String>());
			
			if (!writeTermHash.containsKey(selectedCrawler)){// add new entry in hash table if crawler does not exist
				writeTermHash.put(selectedCrawler, new ArrayList<String>());
			}
			
			if(term.contains("Write:")){//user choose write to text
				if(!writeTermHash.get(selectedCrawler).contains(myTerm)){
					for(int i = 0; i<crawlerTermHash.get(selectedCrawler).size(); i++){//find the term in 3 language
						if (crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[0].equals(myTerm)){
							myTerm = crawlerTermHash.get(selectedCrawler).get(i);
							continue;
						}else if(crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[1].equals(myTerm)){
							myTerm = crawlerTermHash.get(selectedCrawler).get(i);
							continue;
						}else if(crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[2].equals(myTerm)){
							myTerm = crawlerTermHash.get(selectedCrawler).get(i);
							continue;
						}
					}
					writeTermHash.get(selectedCrawler).add(myTerm);
					writeToFile(writeTermHash, selectedCrawler);	//save term in text file
				}
			}else{//change for the number of terms
				JSONArray array = JSONFactoryUtil.createJSONArray();
				if (term.contains("Delete:")){//user choose to delete
					array.put("Delete");
					if (!crawlerTermHash.get(selectedCrawler).isEmpty())//if list is not empty
						for(int i = 0; i<crawlerTermHash.get(selectedCrawler).size(); i++){//find the term in 3 language
							if (crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[0].equals(myTerm)){
								myTerm = crawlerTermHash.get(selectedCrawler).get(i);
								id_lang = "1";
								continue;
							}else if(crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[1].equals(myTerm)){
								myTerm = crawlerTermHash.get(selectedCrawler).get(i);
								id_lang = "2";
								continue;
							}else if(crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[2].equals(myTerm)){
								myTerm = crawlerTermHash.get(selectedCrawler).get(i);
								id_lang = "3";
								continue;
							}
						}
						if(crawlerTermHash.get(selectedCrawler).indexOf(myTerm)!=-1)
							crawlerTermHash.get(selectedCrawler).remove(crawlerTermHash.get(selectedCrawler).indexOf(myTerm));//remove term
				}
				else{//user choose to insert a term
					array.put("Insert");
					if (!crawlerTermHash.get(selectedCrawler).contains(myTerm)){
						crawlerTermHash.get(selectedCrawler).add(myTerm);
					}
				}
				database(crawlerTermHash, selectedCrawler);	//call function to save data
			
				for(int i = 0; i<crawlerTermHash.get(selectedCrawler).size(); i++){
					array.put(crawlerTermHash.get(selectedCrawler).get(i).split("#")[0].split(",")[Integer.parseInt(crawlerTermHash.get(selectedCrawler).get(i).split("#")[1])-1]);
				}
				array.put(myTerm);
				juser.put("jsTerms", array);
				pw.println(juser.toString());
			}
		}
		else if (term.contains("Remove:")){// case, remove crawler
			crawlerTermHash.remove(selectedCrawler);
			File file = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/crawler_" + selectedCrawler + ".txt");
			if(file.delete())
				myLog.info(file.getName() + " is deleted!");
    		else
    			myLog.info("Delete operation is failed.");

			juser.put("RemoveCr", selectedCrawler);
			pw.println(juser);
		}
	}
	
	//function that write the inserted term in text
	public void writeToFile(Hashtable<String, ArrayList<String> > writeTermHash, String selectedCrawler){
		try {
			File file = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/inserted_terms.txt");
			if (!file.exists()) {// if file doesn't exists, then create it
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("Inserted terms:\n\n");
			for(int i = 0; i<writeTermHash.get(selectedCrawler).size(); i++){
				bw.write((i+1) + ")" + writeTermHash.get(selectedCrawler).get(i) + "\n");
			}
			bw.close();
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void database(Hashtable<String, ArrayList<String> > crawlerTermHash, String selectedCrawler){
		try {
			File file = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/crawler_" + selectedCrawler + ".txt");
			if (!file.exists()) {// if file doesn't exists, then create it
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for(int i = 0; i<crawlerTermHash.get(selectedCrawler).size(); i++){
				bw.write(crawlerTermHash.get(selectedCrawler).get(i) + "\n");
			}
			bw.close();
			
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
