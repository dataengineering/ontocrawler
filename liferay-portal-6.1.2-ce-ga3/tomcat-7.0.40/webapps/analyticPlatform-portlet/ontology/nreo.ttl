@prefix : <http://rdf.iit.demokritos.gr/2014/nreo#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@base <http://rdf.iit.demokritos.gr/2014/nreo> .

<http://rdf.iit.demokritos.gr/2014/nreo> rdf:type owl:Ontology ;
                                         
                                         owl:versionInfo "1.0" ;
                                         
                                         dct:creator "Stasinos Konstantopoulos <stasinos@konstant.gr>" ;
                                         
                                         dct:title "Nuclear or Radiological Emergency Ontology (NREO)" ;
                                         
                                         dct:license <http://creativecommons.org/licenses/by/3.0/> ;
                                         
                                         owl:imports <http://www.w3.org/2004/02/skos/core> .


#################################################################
#
#    Annotation properties
#
#################################################################


###  http://rdf.iit.demokritos.gr/2014/nreo#abbreviation

:abbreviation rdf:type owl:AnnotationProperty ;
              
              skos:definition "Abbreviations, acronyms and other short forms."@en ;
              
              rdfs:comment "Abbreviations, acronyms and other short forms."@en ;
              
              rdfs:subPropertyOf skos:altLabel .



###  http://rdf.iit.demokritos.gr/2014/nreo#canonicalLabel

:canonicalLabel rdf:type owl:AnnotationProperty ;
                
                rdfs:comment "The sub-property of skos:prefLabel that gives the most appropriate formal and unambiguous term for expressing a concept."@en ;
                
                skos:definition "The sub-property of skos:prefLabel that gives the most appropriate formal and unambiguous term for expressing a concept."@en ;
                
                rdfs:subPropertyOf skos:prefLabel .



###  http://rdf.iit.demokritos.gr/2014/nreo#inContextAltLabel

:inContextAltLabel rdf:type owl:AnnotationProperty ;
                   
                   rdfs:comment "The sub-property of skos:altLabel that gives alternative forms for referring to a concept that might be in general ambiguous, but can be unambiguously and accurately used in the usual context of the concept. These are typically more general terms that are commonly understood with a restrcited meaning when found in the right context."@en ;
                   
                   skos:definition "The sub-property of skos:altLabel that gives alternative forms for referring to a concept that might be in general ambiguous, but can be unambiguously and accurately used in the usual context of the concept. These are typically more general terms that are commonly understood with a restrcited meaning when found in the right context."@en ;
                   
                   rdfs:subPropertyOf skos:altLabel .



###  http://rdf.iit.demokritos.gr/2014/nreo#informalLabel

:informalLabel rdf:type owl:AnnotationProperty ;
               
               skos:definition "Informal terms often used by the public and/or to the mass media instead of the canonical term."@en ;
               
               rdfs:comment "Informal terms often used by the public and/or to the mass media instead of the canonical term."@en ;
               
               rdfs:subPropertyOf skos:hiddenLabel .



###  http://rdf.iit.demokritos.gr/2014/nreo#obsoleteLabel

:obsoleteLabel rdf:type owl:AnnotationProperty ;
               
               skos:definition "Terms that should be understood but are no longer actively used."@en ;
               
               rdfs:comment "Terms that should be understood but are no longer actively used."@en ;
               
               rdfs:subPropertyOf skos:hiddenLabel .



###  http://rdf.iit.demokritos.gr/2014/nreo#obviousAbbreviation

:obviousAbbreviation rdf:type owl:AnnotationProperty ;
                     
                     rdfs:comment "Established and well-understood abbreviations."@en ;
                     
                     skos:definition "Established and well-understood abbreviations."@en ;
                     
                     rdfs:subPropertyOf :abbreviation .





#################################################################
#
#    Object Properties
#
#################################################################


###  http://rdf.iit.demokritos.gr/2014/nreo#hasPart

:hasPart rdf:type owl:ObjectProperty ;
         
         rdfs:subPropertyOf skos:narrower .



###  http://rdf.iit.demokritos.gr/2014/nreo#hasTreatment

:hasTreatment rdf:type owl:ObjectProperty ;
              
              owl:inverseOf :isTreatmentFor ;
              
              rdfs:subPropertyOf skos:relatedMatch .



###  http://rdf.iit.demokritos.gr/2014/nreo#isTreatmentFor

:isTreatmentFor rdf:type owl:ObjectProperty ;
                
                rdfs:subPropertyOf skos:relatedMatch .



###  http://rdf.iit.demokritos.gr/2014/nreo#isUnitOf

:isUnitOf rdf:type owl:ObjectProperty ;
          
          rdfs:subPropertyOf skos:relatedMatch .



###  http://rdf.iit.demokritos.gr/2014/nreo#partOf

:partOf rdf:type owl:ObjectProperty ;
        
        rdfs:subPropertyOf skos:broader .



###  http://rdf.iit.demokritos.gr/2014/nreo#usesUnit

:usesUnit rdf:type owl:ObjectProperty ;
          
          owl:inverseOf :isUnitOf ;
          
          rdfs:subPropertyOf skos:relatedMatch .





#################################################################
#
#    Classes
#
#################################################################


###  http://rdf.iit.demokritos.gr/2014/nreo#AgentTerm

:AgentTerm rdf:type owl:Class ;
           
           rdfs:subClassOf :NREmergencyManagementTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#BiomedicalTerm

:BiomedicalTerm rdf:type owl:Class ;
                
                rdfs:subClassOf skos:Concept ;
                
                skos:definition "Biomedical terms that include human physiology, medical conditions and treatments."@en ;
                
                rdfs:comment "Biomedical terms that include human physiology, medical conditions and treatments."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#CommunicationToPublicTerm

:CommunicationToPublicTerm rdf:type owl:Class ;
                           
                           rdfs:subClassOf skos:Concept ;
                           
                           rdfs:comment "Terms regarding communication to the public in the event of nuclear or radiological emergencies."@en ;
                           
                           skos:definition "Terms regarding communication to the public in the event of nuclear or radiological emergencies."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#EffectsTerm

:EffectsTerm rdf:type owl:Class ;
             
             rdfs:subClassOf :MedicalConditionTerm ,
                             :NuclearEngineeringTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#FacilitiesTerm

:FacilitiesTerm rdf:type owl:Class ;
                
                rdfs:subClassOf :PreparednessTerm ;
                
                skos:definition "Terms related to facilities used in nuclear and radiological emergencies."@en ;
                
                rdfs:comment "Terms related to facilities used in nuclear and radiological emergencies."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#MeasurementTerm

:MeasurementTerm rdf:type owl:Class ;
                 
                 rdfs:subClassOf :NuclearEngineeringTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#MeasurementUnit

:MeasurementUnit rdf:type owl:Class ;
                 
                 rdfs:subClassOf :NuclearEngineeringTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#MedicalConditionTerm

:MedicalConditionTerm rdf:type owl:Class ;
                      
                      rdfs:subClassOf :BiomedicalTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#MedicalTreatmentTerm

:MedicalTreatmentTerm rdf:type owl:Class ;
                      
                      rdfs:subClassOf :BiomedicalTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#NREmergencyManagementTerm

:NREmergencyManagementTerm rdf:type owl:Class ;
                           
                           rdfs:subClassOf skos:Concept ;
                           
                           skos:definition "Terms about emergency procedures and especially nuclear and radiological emergency procedures."@en ;
                           
                           rdfs:comment "Terms about emergency procedures and especially nuclear and radiological emergency procedures."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#NuclearEngineeringTerm

:NuclearEngineeringTerm rdf:type owl:Class ;
                        
                        rdfs:subClassOf skos:Concept ;
                        
                        rdfs:comment "Terms related to nuclear engineering fundamentals, physics, and the measurement of physical quantities."@en ;
                        
                        skos:definition "Terms related to nuclear engineering fundamentals, physics, and the measurement of physical quantities."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#PhysiologyTerm

:PhysiologyTerm rdf:type owl:Class ;
                
                rdfs:subClassOf :BiomedicalTerm .



###  http://rdf.iit.demokritos.gr/2014/nreo#PreparednessTerm

:PreparednessTerm rdf:type owl:Class ;
                  
                  rdfs:subClassOf skos:Concept ;
                  
                  skos:definition "Terms regarding preparedness for nuclear and radiological emergencies such as available resources and emergency response facilities"@en ;
                  
                  rdfs:comment "Terms regarding preparedness for nuclear and radiological emergencies such as available resources and emergency response facilities"@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#ResourcesTerm

:ResourcesTerm rdf:type owl:Class ;
               
               rdfs:subClassOf :PreparednessTerm ;
               
               skos:definition "Terms related to resources used in nuclear and radiological emergencies."@en ;
               
               rdfs:comment "Terms related to resources used in nuclear and radiological emergencies."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#SpatialTemporalTerm

:SpatialTemporalTerm rdf:type owl:Class ;
                     
                     rdfs:subClassOf :CommunicationToPublicTerm ,
                                     :NREmergencyManagementTerm .





#################################################################
#
#    Individuals
#
#################################################################


###  http://rdf.iit.demokritos.gr/2014/nreo#Becquerel

:Becquerel rdf:type :MeasurementUnit ,
                    owl:NamedIndividual ;
           
           skos:notation "Bq"^^xsd:string ;
           
           skos:prefLabel "Becquerel"@en ;
           
           skos:definition "Measure of the rate of radioactive decay that corresponds to one atomic disintegration per second (1 Bq = 1 sec^-1). It is used as a measure of the amount of a radioactive material."@en ;
           
           skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#BqperKg

:BqperKg rdf:type :MeasurementUnit ,
                  owl:NamedIndividual ;
         
         skos:prefLabel "Becquerel per kilogram"@en ;
         
         skos:definition "The unit of activity per mass of a radioactive material. Defined as Bq/kg."@en .



###  http://rdf.iit.demokritos.gr/2014/nreo#Effect

:Effect rdf:type :EffectsTerm ,
                 owl:NamedIndividual .



###  http://rdf.iit.demokritos.gr/2014/nreo#Equipment

:Equipment rdf:type :ResourcesTerm ,
                    owl:NamedIndividual ;
           
           :partOf :Procedure .



###  http://rdf.iit.demokritos.gr/2014/nreo#Event

:Event rdf:type :SpatialTemporalTerm ,
                owl:NamedIndividual ;
       
       :partOf :Procedure .



###  http://rdf.iit.demokritos.gr/2014/nreo#Gray

:Gray rdf:type :MeasurementUnit ,
               owl:NamedIndividual ;
      
      skos:notation "Gy"^^xsd:string ;
      
      skos:prefLabel "Gray"@en ;
      
      skos:definition "Joules per kilogram (J/kg), the SI unit used for measuring the absorbed dose."@en ;
      
      skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#Location

:Location rdf:type :SpatialTemporalTerm ,
                   owl:NamedIndividual ;
          
          :partOf :Procedure .



###  http://rdf.iit.demokritos.gr/2014/nreo#Medical

:Medical rdf:type :BiomedicalTerm ,
                  owl:NamedIndividual .



###  http://rdf.iit.demokritos.gr/2014/nreo#MedicalCondition

:MedicalCondition rdf:type :MedicalConditionTerm ,
                           owl:NamedIndividual ;
                  
                  skos:broader :Medical .



###  http://rdf.iit.demokritos.gr/2014/nreo#MedicalTreatment

:MedicalTreatment rdf:type :MedicalTreatmentTerm ,
                           owl:NamedIndividual ;
                  
                  skos:broader :Medical .



###  http://rdf.iit.demokritos.gr/2014/nreo#PhysicsThing

:PhysicsThing rdf:type :NuclearEngineeringTerm ,
                       owl:NamedIndividual .



###  http://rdf.iit.demokritos.gr/2014/nreo#PhysiologyItem

:PhysiologyItem rdf:type :PhysiologyTerm ,
                         owl:NamedIndividual ;
                
                skos:broader :Medical .



###  http://rdf.iit.demokritos.gr/2014/nreo#Procedure

:Procedure rdf:type :NREmergencyManagementTerm ,
                    owl:NamedIndividual .



###  http://rdf.iit.demokritos.gr/2014/nreo#Quantity

:Quantity rdf:type :MeasurementTerm ,
                   owl:NamedIndividual .



###  http://rdf.iit.demokritos.gr/2014/nreo#Radiation

:Radiation rdf:type :NuclearEngineeringTerm ,
                    owl:NamedIndividual ;
           
           skos:prefLabel "radiation"@en ;
           
           skos:broader :PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/nreo#Role

:Role rdf:type :AgentTerm ,
               owl:NamedIndividual ;
      
      :partOf :Procedure .



###  http://rdf.iit.demokritos.gr/2014/nreo#SI

:SI rdf:type owl:NamedIndividual ,
             skos:ConceptScheme .



###  http://rdf.iit.demokritos.gr/2014/nreo#Sievert

:Sievert rdf:type :MeasurementUnit ,
                  owl:NamedIndividual ;
         
         skos:notation "Sv"^^xsd:string ;
         
         skos:prefLabel "Sievert" ;
         
         skos:definition "The SI unit of equivalent dose and effective dose. 1 Sv = 1 J/kg."@en ;
         
         skos:example "The average annual radiation dose to the UK population is 2.6 mSv."@en ;
         
         skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#TreatmentAgent

:TreatmentAgent rdf:type :MedicalTreatmentTerm ,
                         owl:NamedIndividual ;
                
                :partOf :MedicalTreatment .



###  http://rdf.iit.demokritos.gr/2014/nreo#centimetre

:centimetre rdf:type :MeasurementUnit ,
                     owl:NamedIndividual ;
            
            skos:notation "cm"^^xsd:string ;
            
            skos:altLabel "centimeter"@en ;
            
            skos:prefLabel "centimetre"@en ;
            
            skos:definition "centimetre (1E-2 m)"@en ;
            
            skos:broader :metre .



###  http://rdf.iit.demokritos.gr/2014/nreo#cps

:cps rdf:type :MeasurementUnit ,
              owl:NamedIndividual ;
     
     skos:prefLabel "counts per second"@en ;
     
     skos:definition "counts per second"@en ;
     
     skos:altLabel "cps"@en ;
     
     skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#manSievert

:manSievert rdf:type :MeasurementUnit ,
                     owl:NamedIndividual ;
            
            skos:notation "manSv"^^xsd:string ;
            
            skos:definition "Person-Sievert"@en ;
            
            skos:prefLabel "man-sievert"@en ;
            
            skos:altLabel "person-sievert"@en ;
            
            skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#metre

:metre rdf:type :MeasurementUnit ,
                owl:NamedIndividual ;
       
       skos:notation "m"^^xsd:string ;
       
       skos:altLabel "meter"@en ;
       
       skos:prefLabel "metre"@en ;
       
       skos:inScheme :SI .



###  http://rdf.iit.demokritos.gr/2014/nreo#microSievert

:microSievert rdf:type :MeasurementUnit ,
                       owl:NamedIndividual ;
              
              skos:notation "μSv"^^xsd:string ;
              
              skos:definition "1E-6 Sievert"@en ;
              
              skos:prefLabel "micro sievert"@en ;
              
              skos:inScheme :SI ;
              
              skos:broader :Sievert .



###  http://rdf.iit.demokritos.gr/2014/nreo#micrometre

:micrometre rdf:type :MeasurementUnit ,
                     owl:NamedIndividual ;
            
            skos:notation "μm"^^xsd:string ;
            
            skos:altLabel "micro meter"@en ,
                          "micro metre"@en ;
            
            skos:definition "micro metre (1E-6 m)"@en ;
            
            skos:altLabel "micrometer"@en ;
            
            skos:prefLabel "micrometre"@en ;
            
            skos:broader :metre .



###  http://rdf.iit.demokritos.gr/2014/nreo#milliSievert

:milliSievert rdf:type :MeasurementUnit ,
                       owl:NamedIndividual ;
              
              skos:notation "mSv"^^xsd:string ;
              
              skos:definition "1E-3 Sievert"@en ;
              
              skos:prefLabel "milli sievert"@en ;
              
              skos:inScheme :SI ;
              
              skos:broader :Sievert .




###  Generated by the OWL API (version 3.4.2) http://owlapi.sourceforge.net

