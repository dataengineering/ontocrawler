<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
 
 @document: 		 view.jsp from file /html/jsps/receiver
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import = "com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import = "javax.portlet.PortletPreferences" %>
<portlet:defineObjects />
<portlet:resourceURL var="getActivationOpt"></portlet:resourceURL>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type='text/javascript' src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>   
	<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>	
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css">
 	<script type="text/javascript" src="<%=request.getContextPath()%>/js/buttons.js"></script>
	<%-- import script for crontab --%>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-cron.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-cron.css"/>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-gentleSelect.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-gentleSelect.css"/>
	
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxcore.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxdata.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxdraw.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxchart.core.js"></script>
	
	<script>
	var crawler = ''; var numOfTerm = 0; var cron_field = '';
	</script>

	<%-- take terms from SenderPortletAction class and insert them to receiver portlet and database --%>
	<script type="text/javascript"> 	
	$(function(){
		cron_field = $('#gentle').cron({// Initialise a cron object
		    useGentleSelect: true // default: false
		});
	});
	</script>
	
	<%-- Take terms from SenderPortletAction class and insert them to receiver portlet and database --%>
	<script>
	Liferay.on('getTermValue',function(event) {// length -1 because there is the term for database in last position
		numOfTerm = event.termValue.jsTerms.length-1;
		if (event.termValue.jsTerms[0] == "Insert" || event.termValue.jsTerms[0] == "Delete") {//if there are change to the number of inserted terms
			$("#termInformation").empty();
			for (var i=1;i<numOfTerm;i++){
				$("#termInformation").append("<li><cl>" + event.termValue.jsTerms[i] + "</cl></li>"); //gather terms
			}
			if(event.termValue.jsTerms[0] == "Insert"){//case of insert a term
				insertTerm('http://localhost:8081/PrepareCrawlers/insert?queries=[{"query_term":"' + event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[0] + '", ' + '"language_id":2},'+
				'{"query_term":"' + event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[1] + '", ' + '"language_id":2},'+
				'{"query_term":"' + event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[2] + '", ' + '"language_id":3}]');
			}
		}});
	</script>
	
	<%-- Insert terms in database using .ajax() calling a url...Jsonp type response--%>
	<script>
	function insertTerm(http_url) {
	    $.ajax({
	        type: "GET",
	        url: http_url,
	        error: function(request, status, error) {alert(status +  '; ' + error);}, 	//error in server response
	        success: function (data) {$(data).each(function (index, value) {if(value.status=="OK"){alert("Term inserted successfully");}});}
	    });
	}</script>

	
	<%-- receive the name of selected crawler and the terms that contains, data are received from CrawlerPortletAction --%>
	<script>
	Liferay.on('getSelectCrawler',function(event){
		$("#crawlerInformation").empty();
		crawler = event.selectCrawler.jsCrawlerTerm[0];					// get the name of selected crawler
		$("#crawlerInformation").append(crawler);						//send the name of selected crawler
		if (event.selectCrawler.jsCrawlerTerm[1] == "Not activate"){	// selected crawler is not activated
			$("#activeTD").empty();
			$("#inActiveTD").empty();
			$("#crawledDoc").empty();
			cron_field.cron("value",event.selectCrawler.jsCrawlerTerm[7]);// Updating the value of an existing cron object
		}else{// selected crawler is already activated
			$("#activeTD").empty();
			$("#activeTD").append(event.selectCrawler.jsCrawlerTerm[1]);
			$("#inActiveTD").empty();
			$("#inActiveTD").append(event.selectCrawler.jsCrawlerTerm[2]);
			$("#crawledDoc").empty();
			$("#crawledDoc").append(event.selectCrawler.jsCrawlerTerm[3] + "</p><p>" + event.selectCrawler.jsCrawlerTerm[4] + "<p>" + event.selectCrawler.jsCrawlerTerm[5] + "</p><p>" + event.selectCrawler.jsCrawlerTerm[6]);
			cron_field.cron("value",event.selectCrawler.jsCrawlerTerm[7]);// Updating the value of an existing cron object
			
			var documentDay = [];
			for (var i=0;i<event.selectCrawler.numDocFig.length;i++){// get the number of the returned document of crawler
				documentDay.push({
					"Day" : i, "Documents" : event.selectCrawler.numDocFig[i]
				});
			}
		}
		$("#termInformation").empty();
		for (var i=8;i<event.selectCrawler.jsCrawlerTerm.length;i++){// get the terms that crawler contains
			$("#termInformation").append("<li><cl>" + event.selectCrawler.jsCrawlerTerm[i] + "</cl></li>"); // gather terms
		}
		figure(documentDay);
	});
	</script>


	<%-- function that estimate the date and time of the crawler activation and active available crawlers --%>
	<script type="text/javascript">
	function activeCrawler(){
		if(crawler != ''){
			var d = new Date();
			var month = (d.getMonth()+1);
			var minutes = d.getMinutes();
			if (month<10){month = '0'+month;}
			if (minutes<10){minutes = '0'+minutes;}
			var activeElemnt = [];
			activeElemnt.push(crawler);
			activeElemnt.push('Active');
			activeElemnt.push('Date:' + d.getFullYear() + '-' + month + '-' + d.getDate() + '  Time:' + d.getHours() + ':' + minutes);
			activeElemnt.push('* * * * *');
			returnCrawlerCond(activeElemnt);//return the list of terms for the specific crawler and update active/crontab time and date
		}else
			alert("You have to select crawler");
	}</script>
	
	<script type="text/javascript">
	function defineSchedule(){
		if(crawler != '' && cron_field.cron("value")!='* * * * *'){
			var activeElemnt = [];
			activeElemnt.push(crawler);
			activeElemnt.push('Active');
			activeElemnt.push('Not available yet');
			activeElemnt.push(cron_field.cron("value"));
			returnCrawlerCond(activeElemnt);//return the list of terms for the specific crawler and update active/crontab time and date		
		}else if(crawler== '')
			alert("You have to select crawler");
		else if(cron_field.cron("value")=='* * * * *')//case without crontab defenition
			alert("You have to define crawler's schedule with crontab");
	}
	</script>
	
	<%-- send items about activation to ReceiverPortletAction class --%>
	<script type="text/javascript">
	function returnCrawlerCond(activeElemnt){
		var myJsonString = JSON.stringify(activeElemnt);
		$.ajax({
			url:'<%=getActivationOpt%>',
			dataType: "json",
			data:{myJsonString:myJsonString},
			type: "get",
			success: function(data){Liferay.fire('getActivationOpt', {myJsonString:data});},
			beforeSend: function(){},	//before send this method will be called
			complete: function(){}		//after completed request then this method will be called.
		});
	}</script>
	
	
	<%-- function that estimate the date and time of the crawler inactivation --%>
	<script type="text/javascript">
	function inActiveCrawler(){
		var activeElemnt = [];
		activeElemnt.push(crawler);
		activeElemnt.push('Inactive');
		activeElemnt.push('Unk');
		activeElemnt.push('No crontab');
		returnCrawlerCond(activeElemnt);//Upadate that crawler is Inactivated
	}</script>
	
	
	<%-- receive date and time from ReciverPortletAction class --%>
	<script>
	Liferay.on('getActivationOpt',function(event) {
		if (event.myJsonString.crawlerCond[1] == "Active"){
			$("#activeTD").empty();
			$("#activeTD").append(event.myJsonString.crawlerCond[2]);
			var stringOfCrawlerTerm = "";								//list with the crawler's term which is being active
			for (var j=4;j<event.myJsonString.crawlerCond.length;j++){	// get the terms that crawler contains
				stringOfCrawlerTerm += '"' + event.myJsonString.crawlerCond[j] + '",';
			}
			if((event.myJsonString.crawlerCond[3] == '* * * * *') && (event.myJsonString.crawlerCond[2] != 'Not available yet')){
				stringOfCrawlerTerm = stringOfCrawlerTerm.substring(0, stringOfCrawlerTerm.length-1);
				crawling('http://localhost:8081/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=true'+
					'&query_list=['+ stringOfCrawlerTerm +']&crawl_name='+event.myJsonString.crawlerCond[0]);
			}
		}else if (event.myJsonString.crawlerCond[1] == "Inactive"){
			$("#inActiveTD").empty();
			$("#inActiveTD").append(event.myJsonString.crawlerCond[2]);
			$("#crawledDoc").empty();
			$("#crawledDoc").append(event.myJsonString.crawlerCond[4] + "</p><p>" + event.myJsonString.crawlerCond[5] + "<p>" + event.myJsonString.crawlerCond[6] + "</p><p>" + event.myJsonString.crawlerCond[7]);
			var documentDay = [];
			for (var i=0;i<event.myJsonString.numDoc.length;i++){// get the number of the returned document of crawler
				documentDay.push({
					"Day" : i, "Documents" : event.myJsonString.numDoc[i]
				});
			}
			figure(documentDay);
		}
	});</script>
	
	<%-- Active crawling using .ajax() calling a url...Jsonp type response--%>
	<script>
	function crawling(http_url) {
	    $.ajax({
	        type: "GET",
	        url: http_url,
	        error: function(request, status, error) {alert(status +  '; ' + error);}, 	//error in server response
	        success: function(data){
	        	alert("Crawling is done");
	        	inActiveCrawler();
	        }
	    });
	}</script>

	
	<%-- Create figure --%>
	<script type="text/javascript">
    function figure(documentDay){
        $(document).ready(function () {
            // prepare jqxChart settings
            var settings = {
                title: "Document per day for crawler: " + crawler,
                description: "",
                enableAnimations: true,
                showLegend: true,
                padding: { left: 5, top: 5, right: 10, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: documentDay,
                xAxis:
                    {
                        textRotationAngle: 0,
                        dataField: 'Day',
                        formatFunction: function (value) {
                            return value.toString();
                        },
                        showTickMarks: true,
                        tickMarksStartInterval: 0,
                        tickMarksInterval: 1,
                        tickMarksColor: '#888888',
                        unitInterval: 1,
                        showGridLines: true,
                        gridLinesStartInterval: 0,
                        gridLinesInterval: 3,
                        gridLinesColor: '#888888',
                        axisSize: 'auto',
                        minValue: 0,
                        maxValue: 23,
                        valuesOnTicks: false
                    },
                colorScheme: 'scheme02',
                seriesGroups:
                    [
                        {
                            type: 'column',
                            valueAxis:
                            {
                                unitInterval: 50,
                                minValue: 0,
                                maxValue: 2000,
                                displayValueAxis: true,
                                description: 'Crawled Documents',
                                axisSize: 'auto',
                                tickMarksColor: '#888888'
                            },
                            series: [
                                    { dataField: 'Documents', displayText: 'Documents', showLabels: true }
                                ]
                        }
                    ]
            };
            // setup the chart
            $('#chartContainer').jqxChart(settings);
        });
    }</script>

</head>
<body>
	<div id="header" style="background-color:CadetBlue;">
	<h1 style="text-align:center;margin-bottom:0;"><u id="crawlerInformation"></u></h1></div>

	<div id="header" style="background-color:LightSlateGray;">
	<h1 style="text-align:center;margin-bottom:0;">Crawl Parameters</h1></div>
	<ul><b id="termInformation" class="context-menu-one box menu-1"></b></ul>
	
	<div id="header" style="background-color:LightSlateGray;">
	<h1 style="text-align:center;margin-bottom:0;">Statistics</h1></div>
	
	<b>Active Since:</b><p id="activeTD"></p>
	<b>Inactive Since:</b><p id="inActiveTD"></p>
	<b>Crawled Documents: </b><p id="crawledDoc"></p><br>

	<a href="#" onClick="activeCrawler()" class="button button-3d-primary button-rounded"><i class="fa fa-refresh"></i>Execute Once</a>
	
	<a href="#" onClick="defineSchedule()" class="button button-3d-action button-pill">Set Execution Schedule</a>

	<br><br>Define the time that will be activated a crawler every day:<br><div id="gentle"></div><br><br>
	
	<div id='chartContainer' style="width:850px; height:500px">
    </div>
</body>
</html>