<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
  		
 @document: 		 view.jsp from file /html/jsps/manage_crawlers
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>

<portlet:defineObjects />
<portlet:resourceURL var="getSelectCrawler"></portlet:resourceURL>
<portlet:resourceURL var="rightSelectCrawler"></portlet:resourceURL>

<!DOCTYPE html>
<html>
<head>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css">
 	<script type="text/javascript" src="<%=request.getContextPath()%>/js/buttons.js"></script>
 	<%@ page import="javax.portlet.PortletPreferences" %>
 	
 	
 	<script>var crawlers = [];</script>

	<%-- catch the event of selection a crawler, send the name of selected crawler to CrawlersPortletAction class --%>
	<script type="text/javascript">
	$(function(){
		$("#newCrawler").delegate("li","mousedown",function(e){
			if( e.button == 0 ){// left mouse button in crawler
				var res = $(this).text().split(")");				// split the list of crawlers
				var selectCrawler = res[res.length-1] + ",left";	// catch the selected crawler
				$.ajax({
					url:'<%=getSelectCrawler%>',
					dataType: "json",
					data:{termCrawl:selectCrawler},
					type: "get",
					success: function(data){
						Liferay.fire('getSelectCrawler', {selectCrawler:data});
					},
					beforeSend: function(){// before send this method will be called
					},
					complete: function(){// after completed request then this method will be called.
					}
				});
			}
			else if( e.button == 2 ){// right mouse button in crawler
				var res = $(this).text().split(")");				// split the list of crawlers
				var RigSelCrawler = res[res.length-1] + ",right" ;	// catch the selected crawler
				$.ajax({
					url:'<%=rightSelectCrawler%>',
					dataType: "json",
					data:{termCrawl:RigSelCrawler},
					type: "get",
					success: function(data){
						Liferay.fire('rightSelectCrawler', {RigSelCrawler:data});
					},
					beforeSend: function(){// before send this method will be called
					},
					complete: function(){// after completed request then this method will be called.
					}
				});
			}
		});
	});
	</script>
	
	
	<%-- receive from sender portlet the crawler that select to remove --%>
	<script>
	Liferay.on('removeCrawler',function(event) {
		removeCrawler(event.termValue.RemoveCr); 	// call function to remove crawler
	});
	</script>
	
	
	<%-- if crawler does not exist, add crawler in the list of available crawlers --%>
	<script type="text/javascript">
	function addExistCrawler(c){
    	if (crawlers.indexOf(c) == -1){// if crawler does not already exist
    		crawlers.push(c);
    		
    		$.each(crawlers, function(i,v) {// dynamic list
           	if (i==(crawlers.length-1)){	// last inserted crawler
           		$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
           	}});
    	}
    	return false;
    }
	
	function removeCrawler(cr){
    	if (crawlers.indexOf(cr) != -1){// if crawler already exist
    		crawlers.splice(crawlers.indexOf(cr),1);
    		
    		$("#newCrawler").empty();
    		$.each(crawlers, function(i,v) {// dynamic list
           		$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
           	});
    	}
    	return false;
    }
	
    function addCrawler(x){	
    	if (crawlers.indexOf(x.crawler.value.trim()) == -1 ){// if crawler does not already exist
    		crawlers.push(x.crawler.value.trim());
    	
   			$.each(crawlers, function(i,v) {// dynamic list
       		if (i==(crawlers.length-1)){	// last inserted crawler
       			$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
       		}});
    	}
    	return false;
    }
    </script>
</head>

<body>
	<span class="button-dropdown" data-buttons="dropdown">
	    <a href="#" class="button button-rounded button-flat-primary"> Crawlers <i class="fa fa-caret-down"></i></a>
	    <ul class="button-dropdown-menu-below">
	    	<b id="newCrawler" class="context-menu-one box menu-1"></b>
	    </ul>
	</span>
	
	
	<%-- take the list of available crawler --%>
	<script>
	<%final File folder = new File("/home/"+System.getProperty("user.name") + "/.PrepareData");
	for (final File fileEntry : folder.listFiles()) {
        if (!fileEntry.isDirectory()) {
            if (fileEntry.getName().contains("crawler_") & !fileEntry.getName().equals("crawler_crontab.txt")){%>
            	addExistCrawler('<%=fileEntry.getName().replaceFirst("crawler_","").replace(".txt","")%>');<%
            }
        }
	}%>
	</script>

	<form action="" method="POST" onsubmit="return addCrawler(this);"><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<b>Crawler Name:</b>
		<input type="text" name="crawler" value=""/><br>
		<input type="submit" value="Create">
	</form>
	
</body>

</html>